﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 6f;
    
    Vector3 movement;
    Animator anim;
    Rigidbody playerRigidbody;
    int floorMask;
    float camRayLength = 100f;

    bool poweredUp = false;
    float powerUpTimer= 0f;
    public PlayerShooting playerShooting;
    public PlayerHealth playerHealth;
    public Slider healthSlider;
    public int currentHealth;

    void Awake()
    {
        floorMask = LayerMask.GetMask("Floor");
        anim = GetComponent <Animator> ();
        playerRigidbody = GetComponent<Rigidbody>();

    }

    void Update()
    {
        if (poweredUp)
        {
            powerUpTimer -= Time.deltaTime;
            if (powerUpTimer <= 0 )
            {
                poweredUp = false;
                playerShooting.timeBetweenBullets = .15f;
            }
        }
    }

    void FixedUpdate()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        Move (h, v);
        Turning();
        Animating(h, v);
    }

    void Move(float h, float v)
    {
        movement.Set (h, 0f, v);
        movement = movement.normalized * speed * Time.deltaTime;

        playerRigidbody.MovePosition (transform.position + movement);
    }

    void Turning()
    {
        Ray camRay = Camera.main.ScreenPointToRay (Input.mousePosition);

        RaycastHit floorHit;

        if(Physics.Raycast (camRay, out floorHit, camRayLength, floorMask))
        {
            Vector3 playerToMouse = floorHit.point - transform.position;
            playerToMouse.y = 0f;

            Quaternion newRotation = Quaternion.LookRotation (playerToMouse);
            playerRigidbody.MoveRotation (newRotation);
        }
    }
    
    void Animating(float h, float v)
    {
        bool walking = h != 0f || v != 0f;
        anim.SetBool("IsWalking", walking);
    }

    void OnTriggerEnter(Collider other)
    {
        if ((other.gameObject.CompareTag("Healthpickup") || other.gameObject.CompareTag("Gunpickup")) && !poweredUp)
        {
            if (other.gameObject.CompareTag("Gunpickup"))
            {
                poweredUp = true;
                powerUpTimer = 5f;
                playerShooting.timeBetweenBullets= .06f;
                Destroy(other.gameObject);
            }
                
            else;// (other.gameObject.CompareTag("Healthpickup"))
            {
                playerHealth.currentHealth+=50;
                if (playerHealth.currentHealth>=101);
                {
                    playerHealth.currentHealth = 100;
                }
                healthSlider.value = playerHealth.currentHealth;
                Destroy(other.gameObject);
            }
            
        }
    }
}
